# PBvariable - README #
---

### Overview ###

The **PBvariable** script collects all the used variables from a selected PHP script and shows them in a list.

### Screenshots ###

![PBvariable - Input Form](development/readme/pbvariable1.png "PBvariable - Input Form")

![PBvariable - Variable List](development/readme/pbvariable2.png "PBvariable - Variable List")

### Setup ###

* Upload the whole directory **pbvariable** to your webhost.
* Edit the configuration section of the file **pbvariable/pbvariable.php**.
* Access the script **pbvariable/pbvariable.php** from a webbrowser.
* Select a PHP script and press the button **Check the file !**.
* The variable list will be displayed.

### Support ###

This is a free script and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBvariable** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
