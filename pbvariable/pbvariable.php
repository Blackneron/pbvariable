<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//         PBvariable - Lists all variables from a PHP script         //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.6 / 09.08.2015                      //
//                                                                    //
//                      Copyright 2015 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //
// This script collects all the used variables from a selected PHP    //
// script and shows them in a list. It also shows how many times each //
// variable was used.                                                 //
// =====================================================================


// =====================================================================
// Specify product information.
// =====================================================================
$copyright = "Copyright &copy; 2015 - <a href=\"http://www.pb-soft.com\" target=\"_blank\">PB-Soft</a>\n";
$software  = "PBvariable";
$version   = "1.6";


// =====================================================================
// Specify a temporary directory for the uploads.
// =====================================================================
// $tempdir = "temp/";    // Unix/Linux host
$tempdir = "c:\\temp\\";  // Windows host


// =====================================================================
// Specify the default directory for checking files on the server.
// =====================================================================
$serverdir = "source/";


// =====================================================================
// Initialize the fail variable.
// =====================================================================
$fail = 0;


// =====================================================================
// Show the html header.
// =====================================================================


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>
      <?php echo $software." ".$version." - This script displays all php variables from a file !\n"; ?>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta name="author" content="Patrick Biegel / PB-Soft">
    <meta name="description" content="<?php echo $software." ".$version; ?> - This script collects all the used variables from a selected PHP script and shows them in a list.">
    <meta name="keywords" content="patrick, biegel, pb-soft, pb.soft.com, pbvariable, variable, tool, script, check, count, display, list, code">
    <style type="text/css">
    <!--

      a:link {
        background-color: #99CC99;
        color:#CC0066;
        font-weight: bolder;
        text-decoration:none;
      }

      a:visited {
        background-color: #99CC99;
        color:#CC0066;
        font-weight: bolder;
        text-decoration:none;
      }

      a:active {
        background-color: #99CC99;
        color:#CC0066;
        font-weight: bolder;
        text-decoration:none;
      }

      a:hover {
        background-color: #99CC99;
        color:#FF3333;
        font-weight: bolder;
        text-decoration:none;
      }

      body {
        background-color: #99CC99;
        color: #003366;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
      }

      table.title {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        padding-top:30px;
        width: 600px;
      }

      td.title {
        font-size: 36px;
        font-style: italic;
        text-align: center;
      }

      td.copyright {
        font-size: 12px;
        font-weight: bolder;
        text-align: center;
      }

      table.form {
        font-size:12px;
        margin-left: auto;
        margin-right: auto;
        text-align: left;
        width: 460px;
      }

      td.input1 {
        background-color: #64B064;
        border:1px solid #458945;
        color: #003366;
        width: 175px;
      }

      td.input2 {
        background-color: #64B064;
        border-bottom:1px solid #458945;
        border-right:1px solid #458945;
        border-top:1px solid #458945;
        color: #003366;
        width: 285px;
      }

      td.sendbutton {
        text-align: center;
      }

      table.info {
        border-top:1px solid #458945;
        font-size:12px;
        margin-left: auto;
        margin-right: auto;
        width: 440px;
      }

      td.fileinfotitle {
        background-color: #64B064;
        border-bottom:1px solid #458945;
        border-left:1px solid #458945;
        border-right:1px solid #458945;
        color: #003366;
        text-align:center;
      }

      td.fileinfo1 {
        background-color: #BFDFBF;
        border-bottom:1px solid #62AE62;
        border-left:1px solid #62AE62;
        border-right:1px solid #62AE62;
        color: #003366;
        text-align: left;
        width: 130px;
      }

      td.fileinfo2 {
        background-color: #BFDFBF;
        border-bottom:1px solid #62AE62;
        border-right:1px solid #62AE62;
        color: #003366;
        text-align: left;
        width: 310px;
      }

      table.result {
        border-top:1px solid #458945;
        font-size:12px;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        width: 440px;
      }

      td.resulttitle1 {
        background-color: #64B064;
        border-bottom:1px solid #458945;
        border-left:1px solid #458945;
        border-right:1px solid #458945;
        color: #003366;
        font-weight: bolder;
        margin-left: auto;
        margin-right: auto;
      }

      td.resulttitle2 {
        background-color: #64B064;
        border-bottom:1px solid #458945;
        border-right:1px solid #458945;
        color: #003366;
        font-weight: bolder;
        margin-left: auto;
        margin-right: auto;
      }

      td.result1 {
        background-color: #BFDFBF;
        border-bottom:1px solid #62AE62;
        border-left:1px solid #62AE62;
        border-right:1px solid #62AE62;
        color: #003366;
        width: 35px;
      }

      td.result2 {
        background-color: #BFDFBF;
        border-bottom:1px solid #62AE62;
        border-right:1px solid #62AE62;
        color: #003366;
        text-align: left;
        width: 415px;
      }

      td.result3 {
        background-color: #BFDFBF;
        border-bottom:1px solid #62AE62;
        border-right:1px solid #62AE62;
        color: #003366;
        width: 50px;
      }

      p.backlink {
        font-size:12px;
        text-align: center;
      }

      p.error {
        font-size:12px;
        text-align: center;
      }

    -->
    </style>
  </head>
  <body>
<?php


// =====================================================================
// Check if the check button was pressed.
// =====================================================================
if (!isset($_POST['check'])) {


  // ===================================================================
  // Display the input form page.
  // ===================================================================
  ?>
    <form name="variableform" id="variableform" enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <table class="title" width="600" border="0" cellspacing="0" cellpadding="8">
        <tr>
          <td class="title" colspan="2">
            <?php echo $software." ".$version."\n"; ?>
          </td>
        </tr>
        <tr>
          <td class="copyright" colspan="2">
            <?php echo $copyright; ?>
          </td>
        </tr>
      </table>
      <br>
      <table class="form" width="460" border="0" cellspacing="0" cellpadding="8">
        <tr>
          <td class="input1">
            File on server to check:
          </td>
          <td class="input2">
            <input type="text" name="serverfile" size="40" value="<?php echo $serverdir; ?>">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="input1">
            Local file to check:
          </td>
          <td class="input2">
            <input type="file" name="localfile">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="sendbutton" colspan="2">
            <input type="reset" name="Reset" value="Reset the form">
            <input type="hidden" name="MAX_FILE_SIZE" value="30000">
            <input type="submit" name="check" value="Check the file !">
          </td>
        </tr>
      </table>
    </form>
<?php


  // ===================================================================
  // Process the selected file and show the result page.
  // ===================================================================
} else {


  // ===================================================================
  // Check if the selected file is a local file or it is on the server.
  // ===================================================================
  if ($_POST['serverfile'] == $serverdir) {


    // =================================================================
    // We check if the filevariable is empty.
    // =================================================================
    if ($_FILES['localfile']['name'] == "") {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      echo "<br>\n";
      echo "<br>\n";
      echo "<p class=\"error\">\n";
      echo "You did not choose a local file !<br>\n";
      echo "</p>\n";


      // ===============================================================
      // Increase the fail flag.
      // ===============================================================
      $fail++;


      // ===============================================================
      // The filevariable is not empty.
      // ===============================================================
    } else {


      // ===============================================================
      // Set the filesize variables.
      // ===============================================================
      $filesize = round((($_FILES['localfile']['size'])/1024), 2);


      // ===============================================================
      // We check if the file exists.
      // ===============================================================
      if ($filesize == 0) {


        // =============================================================
        // Display an error message.
        // =============================================================
        echo "<br>\n";
        echo "<br>\n";
        echo "<p class=\"error\">\n";
        echo "The specified local file does not exist !<br>\n";
        echo "</p>\n";


        // =============================================================
        // Increase the fail flag.
        // =============================================================
        $fail++;


        // =============================================================
        // The file exists.
        // =============================================================
      } else {


        // =============================================================
        // Set the filepath variable.
        // =============================================================
        $filepath = $tempdir . $_FILES['localfile']['name'];


        // =============================================================
        // Set the filename variable.
        // =============================================================
        $filename = $_FILES['localfile']['name'];


        // =============================================================
        // Check if the file was uploaded.
        // =============================================================
        if (is_uploaded_file($_FILES['localfile']['tmp_name'])) {


          // ===========================================================
          // Move file to final destination.
          // ===========================================================
          move_uploaded_file($_FILES['localfile']['tmp_name'], $filepath);


          // ===========================================================
          // The file was not uploaded.
          // ===========================================================
        } else {


          // ===========================================================
          // Display an error message.
          // ===========================================================
          echo "<br>\n";
          echo "<br>\n";
          echo "<p class=\"error\">\n";
          echo "The specified local file was not uploaded !<br>\n";
          echo "</p>\n";


          // ===========================================================
          // Increase the fail flag.
          // ===========================================================
          $fail++;

        } // Check if the file was uploaded.

      } // Check if the file exists.

    } // Check if the file variable is empty.


    // =================================================================
    // The selected file is on the server.
    // =================================================================
  } else {


    // =================================================================
    // We check if the filevariable is empty.
    // =================================================================
    if ($_POST['serverfile'] == "") {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      echo "<br>\n";
      echo "<br>\n";
      echo "<p class=\"error\">\n";
      echo "You did not choose a server file !<br>\n";
      echo "</p>\n";


      // ===============================================================
      // Increase the fail flag.
      // ===============================================================
      $fail++;


      // ===============================================================
      // The file variable is not empty.
      // ===============================================================
    } else {


      // ===============================================================
      // We check if the file or directory does not exists.
      // ===============================================================
      if (!file_exists($_POST['serverfile'])) {


        // =============================================================
        // Display an error message.
        // =============================================================
        echo "<br>\n";
        echo "<br>\n";
        echo "<p class=\"error\">\n";
        echo "The specified server file does not exist !<br>\n";
        echo "</p>\n";


        // =============================================================
        // Increase the fail flag.
        // =============================================================
        $fail++;


        // =============================================================
        // The file or directory exists.
        // =============================================================
      } else {


        // =============================================================
        // We check if the file is in the web directory.
        // =============================================================
        if (strpos($_POST['serverfile'], $_SERVER['DOCUMENT_ROOT']) === false && strpos($_POST['serverfile'], $serverdir) !== 0) {


          // ===========================================================
          // Display an error message.
          // ===========================================================
          echo "<br>\n";
          echo "<br>\n";
          echo "<p class=\"error\">\n";
          echo "This server file is not inside the web directory !<br>\n";
          echo "</p>\n";


          // ===========================================================
          // Increase the fail flag.
          // ===========================================================
          $fail++;


          // ===========================================================
          // The file is in the web directory.
          // ===========================================================
        } else {


          // ===========================================================
          // Set the filepath variable.
          // ===========================================================
          $filepath = $_POST['serverfile'];


          // ===========================================================
          // Set the filename variable.
          // ===========================================================
          $position = strrpos($filepath, "/");
          $filename = substr($filepath, ($position+1));


          // ===========================================================
          // Set the filesize variables.
          // ===========================================================
          $filesize = round((filesize($filepath)/1024), 2);

        } // Check if the file is in the web directory.

      } // Check if the file exists.

    } // Check if the filevariable is empty.

  } // Check if the file is on the server.


  // ===================================================================
  // Check if there were errors.
  // ===================================================================
  if ($fail == 0) {


    // =================================================================
    // Check if the actual source file could not be opened.
    // =================================================================
    if (false === ($opensource = fopen($filepath, "r"))) {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      echo "<br>\n";
      echo "<br>\n";
      echo "<p class=\"error\">\n";
      echo "The source file could not be opened !<br>\n";
      echo "</p>\n";


      // ===============================================================
      // Increase the fail flag.
      // ===============================================================
      $fail++;


      // ===============================================================
      // The actual source file could be opened.
      // ===============================================================
    } else {


      // ===============================================================
      // Initialize the counter for the linenumber and the array to
      // store the variables.
      // ===============================================================
      $name = array();
      $number = array();
      $vartemp = array();


      // ===============================================================
      // Read the content of the actual source file.
      // ===============================================================
      while (!feof($opensource)) {


        // =============================================================
        // Read a line from the actual source file.
        // =============================================================
        $line = fgets($opensource, 1024);


        // =============================================================
        // Checks if it is not a comment line.
        // =============================================================
        if (!preg_match("/^(\\/\\/)+/", $line)) {


          // ===========================================================
          // Checks for variables in the actual line.
          // ===========================================================
          if (preg_match_all('/[\$][a-zA-Z0-9_]+([[][\'"]{0,1}[a-zA-Z0-9_]+[\'"]{0,1}])*/', $line, $vartemp, PREG_SET_ORDER)) {


            // =========================================================
            // Count variables found in the actual line.
            // =========================================================
            $varnumber = count($vartemp);


            // =========================================================
            // Loop through the variables.
            // =========================================================
            for ($key = 0; $key < $varnumber; $key++) {


              // =======================================================
              // Check if the variable is allready in the array.
              // =======================================================
              if (in_array($vartemp[$key][0], $name)) {


                // =====================================================
                // Get the variable index.
                // =====================================================
                $index = array_search($vartemp[$key][0], $name);


                // =====================================================
                // Increase the number of times the variable was used.
                // =====================================================
                $number[$index]++;


                // =====================================================
                // The variable is new.
                // =====================================================
              } else {


                // =====================================================
                // Store the variable into the variable array.
                // =====================================================
                $name[] = $vartemp[$key][0];


                // =====================================================
                // Initialize the variable number counter.
                // =====================================================
                $number[] = 1;

              } // Check if the variable is allready in the array.

            } // Loop through the variables.

          } // Checks for variables in the actual line.

        } // Checks if it is not a comment line.

      } // Loop through the file.


      // ===============================================================
      // Close the open file.
      // ===============================================================
      fclose($opensource);


      // ===============================================================
      // Count the different variables
      // ===============================================================
      $varnumber = (count($name));


      // ===============================================================
      // Sort the array.
      // ===============================================================
      array_multisort($name, $number);


      // ===============================================================
      // Display the variable infos in a table.
      // ===============================================================
?>
    <table class="title" width="300" border="0" cellpadding="6" cellspacing="0">
      <tr>
        <td>
          &nbsp;
        </td>
      </tr>
      <tr>
        <td class="title">
          <?php echo $software." ".$version."\n"; ?>
        </td>
      </tr>
      <tr>
        <td class="copyright">
          <?php echo $copyright; ?>
        </td>
      </tr>
    </table>
    <br>
    <table class="info" width="440" border="0" cellpadding="6" cellspacing="0">
      <tr>
        <td class="fileinfotitle" colspan="2">
          File Information
        </td>
      </tr>
      <tr>
        <td class="fileinfo1">
          Filename:
        </td>
        <td class="fileinfo2">
          <?php echo $filename."\n"; ?>
        </td>
      </tr>
      <tr>
        <td class="fileinfo1">
          Filesize in KB:
        </td>
        <td class="fileinfo2">
          <?php echo $filesize."\n"; ?>
        </td>
      </tr>
      <tr>
        <td class="fileinfo1">
          Variables found:
        </td>
        <td class="fileinfo2">
          <?php echo $varnumber."\n"; ?>
        </td>
      </tr>
    </table>
    <p class="backlink">
      <a href="<?php echo $_SERVER['PHP_SELF']; ?>" target="_self">
        Select another file...
      </a>
    </p>
    <table class="result" width="500" border="0" cellpadding="4" cellspacing="0">
      <tr>
        <td class="resulttitle1">
          No:
        </td>
        <td class="resulttitle2">
          Variable name:
        </td>
        <td class="resulttitle2">
          Used:
        </td>
      </tr>
<?php


      // ===============================================================
      // Loop through the variables found.
      // ===============================================================
      for ($key = 0; $key < $varnumber; $key++) {


        // =============================================================
        // Display the information line for the actual variable.
        // =============================================================
        echo "<tr>\n";
        echo "<td class=\"result1\">\n";
        echo ($key+1)."\n";
        echo "</td>\n";
        echo "<td class=\"result2\">\n";
        echo $name[$key]."\n";
        echo "</td>\n";
        echo "<td class=\"result3\">\n";
        echo $number[$key]."\n";
        echo "</td>\n";
        echo "</tr>\n";

      } // Loop through the variables found.


      // ===============================================================
      // Close the result table.
      // ===============================================================
      echo "</table>\n";

    } // The actual source file could be opened.

  } // Check if there were errors.


?>
    <p class="backlink">
      <a href="<?php echo $_SERVER['PHP_SELF']; ?>" target="_self">
        Select another file...
      </a>
    </p>
<?php


} // Check if the check button was pressed.


?>
  </body>
</html>
